class St:
    def __init__(self, username, password, chatid, first_info=None, info=None, exams=None):
        if info is None:
            info = []
        if exams is None:
            exams = []
        if first_info is None:
            first_info = []
        self.username = username
        self.password = password
        self.chatid = chatid
        self.first_info = first_info
        self.info = info
        self.exams = exams

    def u(self):
        return self.username

    def inf(self):
        return self.info

    def f_info(self):
        return self.first_info

    def exs(self):
        return self.exams

    def p(self):
        return self.password
