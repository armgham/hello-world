# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from bs4 import BeautifulSoup
import nn
import tt


def main(st, bot, update):
    d = webdriver.PhantomJS()
    try:
        d.get("http://erp.guilan.ac.ir")
        elem = d.find_element_by_partial_link_text('ورود به س')
        elem.click()

        wait = WebDriverWait(d, 10)
        elem = wait.until(ec.presence_of_element_located((By.ID, 'iframe_040101')))

        d.get(elem.get_property('src'))
        elem = d.find_element_by_name('SSMUsername_txt')
        elem.send_keys(st.u())

        elem = d.find_element_by_name('SSMPassword_txt')
        elem.send_keys(st.p() + Keys.ENTER)
        elem = wait.until(ec.presence_of_element_located((By.ID, 'userInfoTitle')))
        elem.click()
        elem = wait.until(ec.presence_of_element_located((By.PARTIAL_LINK_TEXT, 'امور آموزش')))
        elem.click()
        elem = wait.until(ec.presence_of_element_located((By.PARTIAL_LINK_TEXT, 'فرم تثب')))
        elem.click()
        elem = wait.until(ec.presence_of_element_located((By.ID, 'iframe_020203')))
        d.get(elem.get_property('src'))
        ic = 11

        soup = BeautifulSoup(d.page_source, 'html.parser')
        print(0)
        es = soup.find_all('table', class_='grd')
        print(1)
        for xx in range(len(es[0].find_all('td'))):
            if es[0].find_all('td')[xx].find('span').text == 'زمان برگزاري':
                ic = xx
        es = es[1:]
        nd = 0
        for xx in range(len(es)):
            try:
                int(es[xx].find_all('td')[0].find('span').text)
                nd = xx

            except ValueError:
                break
        es = es[0:nd + 1]
        print(ic)
        st.first_info = []
        st.exams = []
        st.info = []
        for i in range(len(es)):
            sss = es[i].find_all('td')
            st.first_info.append(
                sss[ic].find('span').text + '\t\t\t' + sss[2].find('span').text + '\t\t(((' + sss[ic - 1].find(
                    'span').text.replace('\n ', ''))
        ie = -1
        for xx in range(len(es[0].find_all('td'))):
            if es[0].find_all('td')[xx].find('span').text == 'زمان امتحان':
                ie = xx
        for i in range(len(es)):
            ssg = es[i].find_all('td')
            st.exams.append(ssg[2].find('span').text + '   :   ' + ssg[ie].find('span').text)
        print(st.f_info())
        nn.main(st, bot, update)
        tt.main(st, bot, update)
    except Exception as e:
        print(e.args)
        bot.send_message(chat_id=update.message.chat_id,
                         text='نمیدونم تو ریدی یا سایت یا من؟! ولی محض اطمینان یه بار دیگه یوزر و پسوردتو درست بفرست اگه نتونستم که دیگه شرمنده.')
    finally:
        d.close()
