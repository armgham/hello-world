import os
import re

import arabic_reshaper
import matplotlib.pyplot as plt
from bidi.algorithm import get_display
import telegram


def ds(day):
    if day == get_display(arabic_reshaper.reshape('شنبه')):
        return 0
    elif day == get_display(arabic_reshaper.reshape('يکشنبه')):
        return 1
    elif day == get_display(arabic_reshaper.reshape('دوشنبه')):
        return 2
    elif day == get_display(arabic_reshaper.reshape('سه شنبه')):
        return 3
    elif day == get_display(arabic_reshaper.reshape('چهارشنبه')):
        return 4
    elif day == get_display(arabic_reshaper.reshape('پنجشنبه')):
        return 5
    return -1


def main(st, bot, update):
    days = []
    starts = []
    ends = []
    coms = []
    darss = []

    colors = ['pink', 'lightgreen', 'lightblue', 'crimson', 'salmon', 'yellow', 'silver', 'yellowgreen', 'y', 'tan',
              'orchid', 'c', 'aqua', 'deeppink']

    for line in st.inf():
        data = line.split('\t')
        days.append(get_display(arabic_reshaper.reshape(data[0])))
        starts.append(float(data[1].split(':')[0]) + float(data[1].split(':')[1]) / 60)
        ends.append(float(data[2].split(':')[0]) + float(data[2].split(':')[1]) / 60)
        if len(data) == 6:
            coms.append(get_display(arabic_reshaper.reshape(data[3])))
        darss.append(get_display(arabic_reshaper.reshape(data[4])))
    days2 = set(days)

    dd = dict((ds(x) - 0.5, x) for x in days2)
    sorted_days = []
    for key in sorted(dd):
        sorted_days.append(dd[key])

    darss = list(set(darss))
    nl = dict((darss[x], x) for x in range(len(darss)))
    mn = min(starts)
    mx = max(ends)
    fig = plt.figure(figsize=(16, 7.89))
    for i, line in zip(range(len(days)), st.inf()):
        data = line.split('\t')

        plt.fill_between([starts[i] + 0.05, ends[i] - 0.05],
                         [sorted_days.index(days[i]) + 0.5, sorted_days.index(days[i]) + 0.5],
                         [sorted_days.index(days[i]) + 1.4, sorted_days.index(days[i]) + 1.4],
                         color=colors[nl[get_display(arabic_reshaper.reshape(data[4]))]], edgecolor='red', linewidth=5)

        plt.text(starts[i] + 0.28, sorted_days.index(days[i]) + 0.55,
                 '{0}:{1:0>2}'.format(int(data[1].split(':')[0]), int(data[1].split(':')[1])), va='top', fontsize=7)
        plt.text(ends[i] - 0.09, sorted_days.index(days[i]) + 0.55,
                 '{0}:{1:0>2}'.format(int(data[2].split(':')[0]), int(data[2].split(':')[1])), va='top', fontsize=7)
        plt.text((starts[i] + ends[i]) * 0.5,
                 (sorted_days.index(days[i]) + 0.5 + sorted_days.index(days[i]) + 1.4) * 0.5 - 0.2,
                 get_display(arabic_reshaper.reshape(data[4])), ha='center', va='center', fontsize=10)
        plt.text((starts[i] + ends[i]) * 0.5,
                 (sorted_days.index(days[i]) + 0.5 + sorted_days.index(days[i]) + 1.4) * 0.5,
                 get_display(arabic_reshaper.reshape(data[3])), ha='center', va='center', fontsize=8)
        plt.text((starts[i] + ends[i]) * 0.5,
                 (sorted_days.index(days[i]) + 0.5 + sorted_days.index(days[i]) + 1.4) * 0.5 + 0.2,
                 get_display(arabic_reshaper.reshape(data[5])), ha='center', va='center', fontsize=8)

    ax = fig.add_subplot(111)
    # bx = fig.add_subplot(211)

    examsb = []
    for line in st.exs():
        pattern = r'^.*(?P<y>\d{4})\/(?P<m>\d{2})\/(?P<d>\d{2}).*$'
        examsb.append(re.search(pattern, line))

    exams = dict(
        (int(k.group('d')) + (int(k.group('m')) - 1) * 31 + (int(k.group('y')) - 1) * 31 * 12, list()) for k in examsb
        if k is not None)

    [exams[int(k.group('d')) + (int(k.group('m')) - 1) * 31 + (int(k.group('y')) - 1) * 31 * 12].append(k.group()) for k
     in examsb if k is not None]
    
    
    jj = 0

    for key in sorted(exams):
        for j in exams[key]:
            plt.text(mx - 0.1, len(days2) + 0.88 + (jj - 1) * 0.25, get_display(arabic_reshaper.reshape(j)), fontsize=11)
            jj += 1

    
    
    ax.set_ylim(len(days2) + 0.5 + jj / 4, 0.4)
    ax.set_xlim(mx, mn)
    ax.set_yticks(range(1, len(days2) + 1))
    ax.set_yticklabels(sorted_days)
    ax.xaxis.grid()
    ax.set_xlabel('Time')
    ax.set_ylabel('Day')
    ax2 = ax.twiny().twinx()
    ax2.set_ylim(ax.get_ylim())
    ax2.set_xlim(ax.get_xlim())
    ax2.set_yticks(ax.get_yticks())
    ax2.set_yticklabels(sorted_days)
    ax2.set_xlabel('Time')
    ax2.set_ylabel('Day')
    bot.send_message(chat_id=update.message.chat.id, text='داره میاد!!')
    bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.UPLOAD_DOCUMENT)
    plt.savefig('{0}.pdf'.format(st.u() + 'barn'))
    plt.savefig('{0}.png'.format(st.u() + 'barn'), dpi=150)
    bot.send_document(chat_id=update.message.chat.id, document=open('{0}.png'.format(st.u() + 'barn'), 'rb'))
    bot.send_document(chat_id=update.message.chat.id, document=open('{0}.pdf'.format(st.u() + 'barn'), 'rb'))
    # plt.show()
    os.remove('{0}.png'.format(st.u() + 'barn'))
    os.remove('{0}.pdf'.format(st.u() + 'barn'))
    bot.send_message(chat_id=update.message.chat.id, text='میتونی حتی به برنامت اضافه هم بکنی:\n''/j')

# main('950121230013')
